﻿namespace ReflectionTests
{
    using System;
    using System.Linq.Expressions;

    public class Startup
    {
        public static void Main()
        {
            Func<int> func = () => 1;
            Expression<Func<MyClass, int>> expr = c => c.SomeMethod();
            var body = expr.Body;
            if (body.NodeType == ExpressionType.Call)
            {
                var methodCallExpression = (MethodCallExpression)body;
                Console.WriteLine(methodCallExpression.Object);
            }
        }
    }
}
